from electronic_shop import Item


class Keyboard(Item):
    SUPPORTED_LANGUAGES = ('EN', 'RU')
    _max_length_name: int = 100

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__language = self.SUPPORTED_LANGUAGES[0]

    @property
    def language(self) -> str:
        return self.__language

    def change_lang(self) -> None:
        supported_languages_count = len(self.SUPPORTED_LANGUAGES)
        current_language_index = self.SUPPORTED_LANGUAGES.index(self.language)
        next_language_index = (current_language_index + 1) % supported_languages_count
        self.__language = self.SUPPORTED_LANGUAGES[next_language_index]
