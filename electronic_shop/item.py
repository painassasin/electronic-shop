from csv import DictReader
from pathlib import Path
from typing import TypeVar

from .exceptions import InstantiateCSVError

TItem = TypeVar('TItem', bound='Item')


class Item:
    """ "
    Класс для представления товара в магазине
    """

    _max_length_name: int = 10
    _csv_path: Path = Path(__file__).parent.joinpath('items.csv')

    pay_rate: float = 1.0
    all: list[TItem] = []  # noqa: A003

    def __init__(self, name: str, price: int | float, quantity: int):
        self.name = name
        self.price = price
        self.quantity = quantity
        self.all.append(self)

    @property
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, value: str) -> None:
        if len(value) > self._max_length_name:
            raise Exception(f'Длина наименования товара превышает {self._max_length_name} символов.')
        self.__name = value

    @property
    def price(self) -> int | float:
        return self.__price

    @price.setter
    def price(self, value: int | float) -> None:
        self.__price = round(value, 2)

    def calculate_total_price(self) -> int | float:
        """
        Получение общей стоимости товара в магазине
        """
        return self.price * self.quantity

    def apply_discount(self) -> None:
        """
        Применить установленную скидку для конкретного товара
        """
        self.price *= self.pay_rate

    @classmethod
    def instantiate_from_csv(cls) -> None:
        """
        Альтернативный способ создания объектов-товаров.
        Метод считывает данные из csv-файла и создает экземпляры класса,
        инициализируя их данными из файла cls._csv_path.
        """
        if not cls._csv_path.exists():
            raise FileNotFoundError(f'Отсутствует файл {cls._csv_path.name}')

        field_names = ('name', 'price', 'quantity')
        try:
            with cls._csv_path.open(encoding='utf-8') as csvfile:
                csv_reader: DictReader = DictReader(csvfile, fieldnames=field_names)
                next(csv_reader)
                for row in csv_reader:
                    price = float(row['price'])
                    if cls.is_integer(price):
                        price = int(price)
                    cls(name=row['name'], price=price, quantity=int(row['quantity']))
        except (KeyError, ValueError):
            raise InstantiateCSVError(f'Файл {cls._csv_path.name} поврежден')

    @staticmethod
    def is_integer(number: int | float) -> bool:
        """Проверяет, является ли число целым."""
        return float(number) == int(number)

    def __repr__(self) -> str:
        return f'{self.__class__.__name__}({self.name}, {self.price:,}, {self.quantity})'

    def __str__(self) -> str:
        return self.name

    def __add__(self, other: TItem) -> int:
        if not isinstance(other, Item):
            raise ValueError('Складывать можно только объекты Item и дочерние от них.')
        return self.quantity + other.quantity
