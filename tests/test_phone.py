import pytest


@pytest.mark.parametrize('number_of_sim', [0, -1], ids=['ноль', 'меньше нуля'])
def test_sim_cards_number(number_of_sim, phone_factory):
    err_msg = 'Количество физических SIM-карт должно быть целым числом больше нуля.'
    with pytest.raises(ValueError, match=err_msg):
        phone_factory.create(number_of_sim=number_of_sim)


def test_add_two_phones(phone_factory):
    phone_1, phone_2 = phone_factory.create_batch(2)
    total_phones = phone_1 + phone_2
    assert total_phones == (phone_1.quantity + phone_2.quantity)


def test_add_phone_and_item(item, phone):
    total_items = phone + item
    assert total_items == (phone.quantity + item.quantity)


def test_add_item_and_phone(item, phone):
    total_items = item + phone
    assert total_items == (item.quantity + phone.quantity)
