import contextlib
import csv
import tempfile
from typing import Iterable

import pytest
from pytest_factoryboy import register

from electronic_shop import Item
from tests.factories import ItemFactory, KeyboardFactory, PhoneFactory

register(ItemFactory)
register(PhoneFactory)
register(KeyboardFactory)


@pytest.fixture()
def _clear_items():
    Item.all.clear()


@pytest.fixture()
def write_csv_items():
    headers = ['name', 'price', 'quantity']

    @contextlib.contextmanager
    def _wrapper(rows: Iterable[tuple[str, str, str]]):
        with tempfile.NamedTemporaryFile(mode='w+') as f:
            writer = csv.writer(f)
            writer.writerow(headers)
            writer.writerows(rows)
            f.seek(0)
            yield f

    return _wrapper
