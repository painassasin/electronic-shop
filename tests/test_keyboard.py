from itertools import cycle

import pytest

from electronic_shop import Keyboard


def test_language_read_only(keyboard, faker):
    with pytest.raises(AttributeError):
        keyboard.language = faker.random_element(keyboard.SUPPORTED_LANGUAGES)


def test_change_lang(keyboard_factory):
    Keyboard.SUPPORTED_LANGUAGES = ('A', 'B', 'C', 'D', 'E')
    keyboard = keyboard_factory.create()
    expected_languages_iterator = cycle(''.join(Keyboard.SUPPORTED_LANGUAGES))

    for _ in range(10):
        assert keyboard.language == next(expected_languages_iterator)
        keyboard.change_lang()
