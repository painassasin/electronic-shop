from pathlib import Path
from unittest.mock import Mock

import pytest

from electronic_shop import Item
from electronic_shop.exceptions import InstantiateCSVError


@pytest.mark.parametrize(
    ('price', 'quantity', 'total_price'),
    [
        (10_000, 20, 200_000),
        (1_000, 0, 0),
    ],
    ids=['товары есть на складе', 'товаров нет на складе'],
)
def test_calculate_total_price(price, quantity, total_price, item_factory):
    item = item_factory(price=price, quantity=quantity)
    assert item.calculate_total_price() == total_price


@pytest.mark.parametrize(
    ('price', 'discount', 'price_with_discount'),
    [
        (10_000, 0, 10_000),
        (10_000, 0.15, 8_500),
        (9999, 0.27, 7_299.27),
    ],
    ids=['без скидки', 'со скидкой', 'округление скидки'],
)
def test_apply_discount(price, discount, price_with_discount, item_factory):
    item = item_factory(price=price)
    Item.pay_rate = 1 - discount

    item.apply_discount()

    assert item.price == price_with_discount


@pytest.mark.usefixtures('_clear_items')
def test_all_items(item_factory):
    Item.all = []
    items = item_factory.create_batch(2)
    assert Item.all == items


def test_invalid_name_length_limit(item_factory, faker):
    err_msg = f'Длина наименования товара превышает {Item._max_length_name} символов.'
    with pytest.raises(Exception, match=err_msg):
        item_factory.create(name=faker.pystr(min_chars=Item._max_length_name + 1))


@pytest.mark.parametrize(
    ('number', 'result'),
    [(10, True), (10.0, True), (10.5, False)],
    ids=['целое', 'без дробной части', 'с дробной частью'],
)
def test_is_integer(number, result):
    Item.is_integer(number)


@pytest.mark.parametrize(
    ('csv_price', 'price'),
    [('10', 10), ('5.5', 5.5), ('7.512', 7.51)],
    ids=['целая цена', 'дробная цена', 'цена округляется до 2 знака'],
)
@pytest.mark.usefixtures('_clear_items')
def test_instantiate_from_csv(csv_price, price, write_csv_items):
    rows = [('test_item', csv_price, '1')]

    with write_csv_items(rows) as f:
        Item._csv_path = Path(f.name)
        Item.instantiate_from_csv()

    assert len(Item.all) == 1
    item = Item.all[0]
    assert item.price == price
    assert isinstance(item.quantity, int)


def test_add_item_and_not_item(item):
    err_msg = 'Складывать можно только объекты Item и дочерние от них.'
    not_item = Mock()

    with pytest.raises(ValueError, match=err_msg):
        _ = item + not_item


def test_csv_not_found(faker):
    file_path = faker.file_path(depth=1, extension='csv')
    *_, file_name = file_path.rsplit('/')
    Item._csv_path = Path(file_path)

    with pytest.raises(FileNotFoundError, match=f'Отсутствует файл {file_name}'):
        Item.instantiate_from_csv()


def test_csv_file_error(write_csv_items, item_factory):
    item = item_factory.build()
    rows = [(item.name, item.price, 'not_integer_quantity')]

    with write_csv_items(rows) as f:
        Item._csv_path = Path(f.name)

        *_, file_name = f.name.rsplit('/')
        with pytest.raises(InstantiateCSVError, match=f'Файл {file_name} поврежден'):
            Item.instantiate_from_csv()
