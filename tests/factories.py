import factory

from electronic_shop import Item, Keyboard, Phone


class ItemFactory(factory.Factory):
    name = factory.Faker('pystr', max_chars=Item._max_length_name)
    price = factory.Faker('pyint', min_value=1)
    quantity = factory.Faker('pyint', min_value=1)

    class Meta:
        model = Item


class PhoneFactory(ItemFactory):
    number_of_sim = factory.Faker('pyint', min_value=1, max_value=3)

    class Meta:
        model = Phone


class KeyboardFactory(ItemFactory):
    name = factory.Faker('pystr', max_chars=Keyboard._max_length_name)

    class Meta:
        model = Keyboard
